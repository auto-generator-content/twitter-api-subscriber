# twitter-api-subscriber

## Introduction

Contains a basic approach to use the Twitter API.

The script receives the queue name by parameter and get the content from a RabbitMQ queue.

After that, the content is posted in to the twitter account.

## Parameters

- **-queue_name**: Contains the queue name to get the content from RabbitMQ.
- **-thread**: Contains the flag to activate the thread mode.

## Usage

### Docker

- Build the image
  `docker build --build-arg TWITTER_CONSUMER_KEY_TEST --build-arg TWITTER_CONSUMER_SECRET_TEST --build-arg TWITTER_ACCESS_TOKEN_TEST --build-arg TWITTER_TOKEN_SECRET_TEST -t twitter-api .`

- Run the image
  `docker run twitter-api-subscriber -queue_name QUEUE_NAME -thread`

### Python

To use this script you can use the following command:

`python twitter-api-subscriber.py -queue_name QUEUE_NAME -thread`

**NOTE**: Remember that you need to install the **tweepy** Python library and set up the API KEY as environment variable.

## Contact

If you have any question, you can contact me directly to `jherreroa23@gmail.com`
