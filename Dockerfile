FROM python:3.10

ADD twitter_api.py /
ADD requirements.txt /
RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "./twitter_api_subscriber.py" ]
