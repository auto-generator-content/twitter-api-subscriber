import os
import time
import pika
import tweepy
import logging
import argparse

from typing import List

"""
Attributes: 

args.queue_name: Represents the queue name to send the data to RabbitMQ
args.thread: Boolean value to indicate if thread parsing is required

"""


def parse_arguments() -> argparse.Namespace:
    """
    Read arguments from a command line.

    Parameters
    ----------
    Not arguments required.

    Returns
    -------
    :argparse.Namespace
        Contains the structure with the arguments content.

    """
    parser = argparse.ArgumentParser(
        description='Arguments get parsed via --commands')
    parser.add_argument(
        '-queue_name',
        metavar='queue_name',
        type=str,
        required=True,
        help='This must contain the queue name to get the tweet')
    parser.add_argument(
        '-thread',
        action='store_true',
        help='This indicates if thread processing should be done')
    return parser.parse_args()


def get_tweet_queue(queue_name: str) -> None:
    """
    Given the queue_name, gets a tweet to be published.

    Parameters
    ----------
    queue_name: str
        Represents the queue where to send messages

    Returns
    -------
    None
    """
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='host.docker.internal'))
    channel = connection.channel()

    channel.queue_declare(queue=queue_name)
    channel.basic_qos(prefetch_count=1)

    def callback(ch, method, properties, body):
        tweet = body.decode('utf-8')
        logging.info(f'Received tweet: {tweet}')
        ch.basic_ack(delivery_tag=method.delivery_tag)
        ch.stop_consuming()
        publish_tweet(tweet)

    channel.basic_consume(
        queue=queue_name, on_message_callback=callback)

    logging.info(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


def get_thread_queue(queue_name: str) -> str:
    """
    Given a queue_name returns all the tweets that compose
    the Twitter thread.

    Parameters
    ----------
    queue_name: str
        Represents the queue where to send messages

    Returns
    -------
    None
    """
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='host.docker.internal'))
    channel = connection.channel()

    channel.queue_declare(queue=queue_name)

    thread = []
    # Keep consuming messages until the queue is empty
    while True:
        method_frame, header_frame, body = channel.basic_get(
            queue=queue_name, auto_ack=False)
        if method_frame:
            logging.info(f"Getting message: {body.decode('utf-8')}")
            thread.append(body.decode('utf-8'))
            channel.basic_ack(method_frame.delivery_tag)
        else:
            logging.info("There are no more messages in the queue.")
            break

    publish_thread(thread)


def publish_tweet(message: str) -> None:
    """
    Given the message, it publishes the content to the
    Twitter account.

    Parameters
    ----------
    message: str
        Contains the tweet content.

    Returns
    -------
    None
    """
    consumer_key = os.environ['TWITTER_CONSUMER_KEY_TEST']
    consumer_secret = os.environ['TWITTER_CONSUMER_SECRET_TEST']
    access_token = os.environ['TWITTER_ACCESS_TOKEN_TEST']
    access_token_secret = os.environ['TWITTER_TOKEN_SECRET_TEST']

    # Authenticate to Twitter
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    try:
        api.update_status(status=message)
    except Exception as error:
        logging.error(f'Error. The tweet has not been posted: {error}')


def publish_thread(thread: List) -> None:
    """
    Given the list of messages, it publishes the content to the
    Twitter account.

    Parameters
    ----------
    message: str
        Contains the tweet content.

    Returns
    -------
    None
    """
    consumer_key = os.environ['TWITTER_CONSUMER_KEY_TEST']
    consumer_secret = os.environ['TWITTER_CONSUMER_SECRET_TEST']
    access_token = os.environ['TWITTER_ACCESS_TOKEN_TEST']
    access_token_secret = os.environ['TWITTER_TOKEN_SECRET_TEST']

    # Authenticate to Twitter
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)

    api = tweepy.API(auth)

    try:
        tweet_id = api.update_status(status=thread[0]).id
        for tweet in thread[1:]:
            tweet_id = api.update_status(
                status=tweet, in_reply_to_status_id=tweet_id).id
            time.sleep(10)
    except Exception as error:
        logging.error(f'Error. The tweet has not been posted: {error}')


def main(args: argparse.Namespace = None) -> None:
    """
    Main function for our code.

    Parameters
    ----------
    args: argparse.Namespace
        Contains the arguments content (if exists)

    Returns
    -------
    None
    """
    if not args.thread:
        get_tweet_queue(args.queue_name)
    else:
        get_thread_queue(args.queue_name)


if __name__ == "__main__":
    logging.basicConfig(level='INFO')
    logging.info('Initializing Twitter Script')
    args = parse_arguments()
    main(args)
    logging.info('Twitter Script end')
